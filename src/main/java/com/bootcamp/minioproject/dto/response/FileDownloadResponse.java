package com.bootcamp.minioproject.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDownloadResponse {
  private byte[] fileData;
  private String fileName;
}