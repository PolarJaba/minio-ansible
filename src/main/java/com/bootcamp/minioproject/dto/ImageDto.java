package com.bootcamp.minioproject.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImageDto implements Serializable {
  private String name;
  private long size;
  private String link;
}
