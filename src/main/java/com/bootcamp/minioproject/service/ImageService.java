package com.bootcamp.minioproject.service;

import com.bootcamp.minioproject.domain.ImageT;
import com.bootcamp.minioproject.domain.User;
import com.bootcamp.minioproject.dto.ImageDto;
import com.bootcamp.minioproject.dto.response.FileDownloadResponse;
import com.bootcamp.minioproject.dto.response.GetImagesResponse;
import com.bootcamp.minioproject.dto.response.Image;
import com.bootcamp.minioproject.dto.response.UiSuccessContainer;
import com.bootcamp.minioproject.dto.response.UploadImageResponse;
import com.bootcamp.minioproject.exceptions.ImageNotFoundException;
import com.bootcamp.minioproject.mapper.ImagesMapper;
import com.bootcamp.minioproject.repository.ImageRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ImageService {

  private final ImageRepository repository;
  private final UserService userService;
  private final MinioService service;
  private final ImagesMapper mapper;

  public boolean existsAll(List<UUID> imageIds) {
    return repository.existsImagesByIdIn(imageIds);
  }

  public FileDownloadResponse downloadImage(UUID imageId) throws Exception {
    User currUser = userService.getCurrentUser();
    Optional<ImageT> image = repository.findImageById(imageId);
      if (image.isEmpty() || !image.get().getUserId().equals(currUser.getId())) {
          throw new ImageNotFoundException("Файл не найден в системе или недоступен");
      }
    byte[] fileData = service.downloadImage(image.get().getLink());
    String fileName = image.get().getName();
    return new FileDownloadResponse(fileData, fileName);
  }

  public UploadImageResponse uploadImage(MultipartFile file) throws Exception {
    ImageDto imageDto = service.uploadImage(file);
    ImageT imageT = new ImageT().setName(imageDto.getName()).setSize(imageDto.getSize())
        .setLink(imageDto.getLink()).setUserId(userService.getCurrentUser().getId())
        .setId(UUID.randomUUID());
    imageT = repository.save(imageT);
    return new UploadImageResponse(imageT.getId());
  }

  public UiSuccessContainer deleteImage(UUID imageId) {
    User currUser = userService.getCurrentUser();
    Optional<ImageT> image = repository.findImageById(imageId);
      if (image.isEmpty() || !image.get().getUserId().equals(currUser.getId())) {
          throw new ImageNotFoundException("Файл не найден в системе или недоступен");
      }
    repository.deleteById(imageId);
      if (!repository.existsImageById(imageId)) {
          return new UiSuccessContainer(true);
      }
    return new UiSuccessContainer(false);
  }

  public Object getImages() {
    User currUser = userService.getCurrentUser();
    List<ImageT> imagesT = repository.findAllByUserId(currUser.getId());
    List<Image> images = mapper.imagesToImagesDto(imagesT);
    return new GetImagesResponse(images);
  }
}