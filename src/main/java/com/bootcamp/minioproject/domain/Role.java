package com.bootcamp.minioproject.domain;

public enum Role {
  ROLE_USER,
  ROLE_ADMIN
}